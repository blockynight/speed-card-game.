# This file contains all the classes for the speed card game.

import random


class CardOperations(object): 
# Deals with anything concerning cards, dealing them ect.
    
    def __init__(self, deck, special_cards):
        
        self.deck = deck
        self.special_cards = special_cards
        self.last_card = ""
        self.last_card_word = ""
        self.last_card_num = ""
        self.used_cards = []
        
        
    def deal_to(self, whos_hand):
        
        while len(whos_hand) < 7:
            
            self.give_card(whos_hand)     
        
    
    def give_card(self, whos_hand):           
        """Adds a card from the deck to someones hand, no duplicate cards are added.
           If the deck is empty all the cards that were played are added back to the deck. """
                                              
        if len(self.deck) == 0:
            print "refill"
            for card in self.used_cards:
                self.deck.append(card)
                
            self.used_cards = []
        
        else:        
            pass
            
        a_card = random.choice(self.deck)
        whos_hand.append(a_card)
        self.deck.remove(a_card)   
    
    
    def first_card(self):      
        """Gets a card from the deck to be used as the first card for the game.
           If the card selected can't be used, it's kept in the deck.          """
        
        loop_again = True

        while loop_again == True:
            a_card = random.choice(self.deck)
            
            if a_card not in self.special_cards:
                self.last_card = a_card
                self.last_card_word, self.last_card_num = self.split_card(self.last_card)
                self.used_cards.append(a_card)
                self.deck.remove(a_card)                
                loop_again = False
                
            else:
                pass  


    def use_card(self, whos_hand, input_card):        
        """Makes the last_card equal to the input card, splits the last card,
           removes the input card from the hand of the person who played it."""
        
        self.last_card = input_card
        self.last_card_word, self.last_card_num = self.split_card(self.last_card) 
        self.used_cards.append(input_card)
        whos_hand.remove(input_card)       
            
            
    def split_card(self, card):
    
        card_split = card.split()     
        card_word = card_split[0]
        card_num = card_split[1]
        
        return  card_word, card_num


class PlayerSystem(object):     # Class for getting input from the player.

    def __init__(self, card_op, player_hand, special_cards):
        
        self.card_op = card_op
        self.hand = player_hand
        self.special_cards = special_cards
        self.abrev = {"m1":"motor 1", "t1":"train 1", "s1":"ship 1", "p1":"plane 1", "l1":"lightning 1",   
                      "m2":"motor 2", "t2":"train 2", "s2":"ship 2", "p2":"plane 2", "l2":"lightning 2",
                      "m3":"motor 3", "t3":"train 3", "s3":"ship 3", "p3":"plane 3", "l3":"lightning 3",
                      "m4":"motor 4", "t4":"train 4", "s4":"ship 4", "p4":"plane 4", "l4":"lightning 4",
                      "m5":"motor 5", "t5":"train 5", "s5":"ship 5", "p5":"plane 5", "p":"pass",
                      "m6":"motor 6", "t6":"train 6", "s6":"ship 6", "p6":"plane 6",
                      "m7":"motor 7", "t7":"train 7", "s7":"ship 7", "p7":"plane 7",
                      "m8":"motor 8", "t8":"train 8", "s8":"ship 8", "p8":"plane 8",
                      "m9":"motor 9", "t9":"train 9", "s9":"ship 9", "p9":"plane 9"}

    
    def player_input(self):  
        """Asks the player what card from their hand they want to play
           and makes sure that card is playable."""
        
        print "Last played card:", self.card_op.last_card
        print "Your hand:", self.hand
        
        loop_again = True
        
        while loop_again == True:
            
            print "Type the card from your hand that you want to play."
            input_card = raw_input("> ").strip().lower()        
            
            if input_card in self.abrev:            # Makes useing abrevations possible.
                input_card = self.abrev[input_card]
                
            else:
                pass
            
            if input_card == "pass":
                self.card_op.give_card(self.hand)
                print "You draw a card as a penalty."
                print "Last played card:", self.card_op.last_card
                print "Your hand:", self.hand
                loop_again = False
            
            elif input_card in self.hand:
                
                input_card_word, input_card_num = self.card_op.split_card(input_card)
                
                check_1 = input_card_word == self.card_op.last_card_word 
                check_2 = input_card_num == self.card_op.last_card_num
                
                if self.card_op.last_card in self.special_cards:
                    self.card_op.use_card(self.hand, input_card)
                    self.check_extra_turn()
                    loop_again = False
                
                elif input_card in self.special_cards:   
                    if input_card != "extra_turn 1":                    
                        self.card_op.use_card(self.hand, input_card)
                        loop_again = False
                        
                    else:
                        print "You can't play that card now!"
                        print "If you can't play any cards type 'pass'.\n"
                        print "Last played card:", self.card_op.last_card
                        print "Your hand:", self.hand
                
                elif check_1 or check_2:
                    self.card_op.use_card(self.hand, input_card)
                    self.check_extra_turn()
                    loop_again = False
                
                else:
                    print "\nFix your typos! or play a valid card!"
                    print "If you can't play any cards type 'pass'.\n"
                    print "Last played card:", self.card_op.last_card
                    print "Your hand:", self.hand
                
            else:
                print "\nPlay a card that's in your hand!"
                print "If you can't play any cards type 'pass'.\n"
                print "Last played card:", self.card_op.last_card
                print "Your hand:", self.hand
            
            
    def check_extra_turn(self):
        
        loop_again = True
        
        while loop_again == True:
            
            if "extra_turn 1" in self.hand:
                print "You have the special card 'extra turn'!"
                print "Do you want to play it now? [y/n]"
            
                next = raw_input("> ")
                
                if next == "y":
                    self.card_op.use_card(self.hand, "extra_turn 1")
                    print "You got rid of an extra card this turn!"
                    loop_again = False        
                
                elif next == "n":
                    print "I'm confused..."
                    loop_again = False
                    
                else:
                    print "I don't understand you..."
                
            else:
                loop_again = False
                
                
class AISystem(object):  # The artifual inteligance for the speed card game.

    def __init__(self, card_op, npc_hand, special_cards):  
    
        self.card_op = card_op
        self.hand = npc_hand
        self.special_cards = special_cards        
        self.last_AI_card = '' 
        
        self.playable_cards = []      
        self.AI_cards = []
        self.lightning_cards = []
        
        self.motor_cards = []
        self.train_cards = []
        self.ship_cards = []
        self.plane_cards = []
        
        self.name = ''
        self.gender = ''        
        
        
    def scan_hand(self):
        """Looks through the npc's hand to find any playable cards and add them to
           an appropriot list."""
        
        self.playable_cards = []      
        self.AI_cards = []
        self.lightning_cards = []        
        self.motor_cards = []
        self.train_cards = []
        self.ship_cards = []
        self.plane_cards = []
        
        if self.last_AI_card != "":
            last_AI_card_word, not_used = self.card_op.split_card(self.last_AI_card)

        else:
            last_AI_card_word = ""

        for card in self.hand:
            
            card_word, card_num = self.card_op.split_card(card)
            
            if card_word == "lightning":
                self.lightning_cards.append(card) 
            
            elif self.card_op.last_card in self.special_cards:
                self.add_to_list(card, card_word, last_AI_card_word)
                
            elif card_word == self.card_op.last_card_word:
                self.add_to_list(card, card_word, last_AI_card_word)
                    
            elif card_num == self.card_op.last_card_num:        
                self.add_to_list(card, card_word, last_AI_card_word)
                
            else:
                pass
    
                               
    def choose_cards(self):           
        """Decides what playable cards should be played."""        
        
        card_lists = [self.motor_cards, self.train_cards, self.ship_cards, self.plane_cards]
        
        if len(self.AI_cards) != 0: 
            for card in self.AI_cards:
                self.playable_cards.append(card)
                
        else:
            for a_list in card_lists:    
                list_len = len(a_list)
                if list_len == 1:
                    for card in a_list:
                        self.playable_cards.append(card)
                        
                else:
                    pass
                    
            if len(self.playable_cards) == 0:
                for a_list in card_lists:                
                    for card in a_list:
                        self.playable_cards.append(card)
                
                if len(self.playable_cards) == 0 and len(self.lightning_cards) != 0:
                    for card in self.lightning_cards:
                        self.playable_cards.append(card)
                        
                elif len(self.playable_cards) == 0:
                    self.playable_cards.append("pass")
                    
                else:
                    pass
                            
            else:
                pass             
    
    
    def play_card(self):
        
        if "pass" in self.playable_cards:
            print "%s couldn't play any cards so %s passed." % (self.name, self.gender)
            self.card_op.give_card(self.hand)            
        
        else:
            input_card = random.choice(self.playable_cards)         
            self.last_AI_card = input_card
            self.card_op.use_card(self.hand, input_card)
            print "%s played:" % self.name, input_card 
                
            if "extra_turn 1" in self.hand:
                print "%s also had an 'extra_turn' card and played it." % self.name
                self.card_op.use_card(self.hand, "extra_turn 1")
                    
            else:
                pass                
                
                                   
    def add_to_list(self, card ,card_word, last_AI_card_word):

        if card_word == last_AI_card_word:
            self.AI_cards.append(card)
                
        elif card_word == "motor":
            self.motor_cards.append(card)
                
        elif card_word == "train":
            self.train_cards.append(card)
                
        elif card_word == "ship":
            self.ship_cards.append(card)
                
        else:
            self.plane_cards.append(card)
            
            
    def who_am_i(self):
        """A little thing to give some life to the game."""
        
        names = ["John", "Kate", "Tom", "Brian", "Emma", "Hellen", "Jordan", "Liam"]
        male_names = ["John", "Tom", "Brian", "Liam"]
        
        self.name = random.choice(names)
        
        if self.name in male_names:
            self.gender = "he"
            
        else:
            self.gender = "she"





