# A version of the speed card game pepys series, a predecessor to UNO, but simpiler and with  different rules, 
# the first edition was published in 1938.

# This is the games main file and calls functions/methods from speed_card_game_classes.py

import speed_card_game_classes
import random
import time
import sys


def start():
    
    staggered_print("", "\n***WELCOME TO THE****", .15)   
    staggered_print("", "***SPEED CARD GAME***", .15) 
    staggered_print("", "****PEPYS SERIES*****", .15)
    
    print "\nTo read how to play type 'help'."
    print "For more information on the game this game is based on type 'info'."
    print "To skip to the game press enter."
    
    next = raw_input("> ")   # Check for badly formed input.
    
    if next == "help":
        print "\n***HOW TO PLAY***"
        print "At the start of the game you will be given 7 cards,"
        print "with names like 'motor 5', 'plane 9', 'ship 4' ,'lightning 1'."
        
        print "\nThere is 4 types of normal cards, motor, train, ship and plane"
        print "and 2 types of special cards, lightning and extra_turn."
        print "There is 9 normal cards of each type and each is numbered 1 to 9."
        print "There is 4 lightning cards numbered 1 to 4"
        print "and 1 extra turn card numbered 0."
        
        print "\nTo win the game you have to be the first player to empty their hand."
        print "To do this you look at the last played card, say 'plane 7'."
        print "You can play any normal card that is the same type as 'plane 7',"
        print "so any card with 'plane' in the name, or you can play any"
        print "normal card with '7' in the name."
        print "Examples are 'plane 3', 'train 7', 'ship 7'.\n"
        
        raw_input("Press enter to continue.")
        
        print "\n'Lightning' cards can be played when you don't have any normal cards to play."        
        print "If you have an 'extra_turn' card in your hand you will be asked if you want to"
        print "play it after you played another card."
        print "If the last played card was a 'lightning' or 'extra_turn' card" 
        print "you can play any normal card."
        print "If you have no cards that you can play (normal or lightning),"
        print "you can type 'pass' to skip your go and be given another card from the deck."
        
        print "\nTo avoid straining your fingers :P, you can abreviate the names of cards,"
        print "when you're typing them in to the first letter of the name of the card"
        print "followed by the number on the card. For example 'p5' becomes 'plane 5',"
        print "'m1' becomes 'motor 1', 's9' becomes 'ship 9' ,'p' becomes 'pass'.\n" 
        
        raw_input("Press enter to continue.")
        
    elif next == "info":
        print "\n***INFO***"
        print "You are about to play a version of the speed card game pepys series,"
        print "a predecessor to UNO, but simpiler and with  different rules,"
        print "the first edition was published in 1938."
        
        raw_input("Press enter to continue.")
        
    else:
        pass
    
    main()

    
def main():
    """Reset everything on starting a new game."""
 
    string_1 = "\nGame starting"
    string_2 = "......"
    staggered_print(string_1, string_2, .35)
    
    deck = gen_deck()
    special_cards = ("lightning 1", "lightning 2", "lightning 3", "lightning 4",
                     "extra_turn 1")
    
    player_hand = []
    npc_1_hand = []
    npc_2_hand = []    
    
    card_op = speed_card_game_classes.CardOperations(deck, special_cards)
    
    card_op.deal_to(player_hand)
    card_op.deal_to(npc_1_hand)
    card_op.deal_to(npc_2_hand)
    
    card_op.first_card()
    
    player = speed_card_game_classes.PlayerSystem(card_op, player_hand, special_cards)
    npc_1 = speed_card_game_classes.AISystem(card_op, npc_1_hand, special_cards)
    npc_2 = speed_card_game_classes.AISystem(card_op, npc_2_hand, special_cards)
    
    npc_1.who_am_i()
    npc_2.who_am_i()
    
    whos_turn_first(player, npc_1, npc_2)


def gen_deck():
    """Generates the deck for the game."""

    build_cards = (lambda l, c, n: l if n < 1 
                   else build_cards(l + [c + " " + str(n)], c, n-1))
    # Returns a list of a "cardName n" where n is an int and counts down to one
    
    normal_deck = map(lambda x: build_cards([], x[1:], int(x[0])), 
                      ["8motor", "9train", "8ship", "9plane",
                       "4lightning", "1extra_turn"])
    # The number of cards wanted is the first char of each string
    # eg "9animal", 9 is the number of cards created
                 
    return [card for card_list in normal_deck 
            for card in card_list]
    
    
def whos_turn_first(player, npc_1, npc_2):
        
        x = random.randint(1, 3)
        
        if x == 1:
            player_turn(player, npc_1, npc_2, "")      
            
        elif x == 2:
            npc_1_turn(player, npc_1, npc_2, "")
            
        else:
            npc_2_turn(player, npc_1, npc_2, "")
              
                  
def player_turn(player, npc_1, npc_2, turn):
    
    turn = "player"
    print "\nIt's your turn."
    player.player_input()
    
    check_win(player, npc_1, npc_2, turn)
    

def npc_1_turn(player, npc_1, npc_2, turn):    
    
    string_1 = "\nIt's %s's turn" % npc_1.name
    string_2 = "......"
    staggered_print(string_1, string_2, .35)
    
    turn = "npc 1"
    npc_1.scan_hand()
    npc_1.choose_cards()
    npc_1.play_card()
    
    check_win(player, npc_1, npc_2, turn)
    
    
def npc_2_turn(player, npc_1, npc_2, turn):
    
    string_1 = "\nIt's %s's turn" % npc_2.name
    string_2 = "......"
    staggered_print(string_1, string_2, .35)
        
    turn = "npc 2"
    npc_2.scan_hand()
    npc_2.choose_cards()
    npc_2.play_card()
    
    check_win(player, npc_1, npc_2, turn)
    
    
def check_win(player, npc_1, npc_2, turn):
    
    if len(player.hand) == 0:
        win()
        
    elif len(npc_1.hand) == 0:
        print "%s won." % npc_1.name
        lose()
        
    elif len(npc_2.hand) == 0:
        print "%s won." % npc_2.name
        lose()
        
    else:
        if turn == "player":
            npc_1_turn(player, npc_1, npc_2, turn)
            
        elif turn == "npc 1":
            npc_2_turn(player, npc_1, npc_2, turn)
            
        else:
            player_turn(player, npc_1, npc_2, turn) 
 
 
def win():

    print "\nYou won!"
    print "Good job!"

    loop_again = True
    
    while loop_again == True:    

        print "Do you want to play again? [y/n]"
        
        next = raw_input("> ")
        
        if next == "y":
            loop_again = False
            main()
            
        
        elif next == "n":
            loop_again = False
            print "Bye bye, I hope you had a fun time."
            sys.exit()
            
        else:
            print "Please type 'y' or 'n' as an answer"


def lose():
    
    print "\nTo bad the computer beat you!"
    print "Better luck next time."
    
    loop_again = True
    
    while loop_again == True:    

        print "Do you want to play again? [y/n]"
        
        next = raw_input("> ")
        
        if next == "y":
            loop_again = False
            main()
            
        
        elif next == "n":
            loop_again = False
            print "\nBye, hope you had a fun time."
            sys.exit()
            
        else:
            print "\nI don't understand you..."    
    

def staggered_print(string_1, string_2, t):
    """Prints string 1 to the screen, prints string 2 on the same
       line as string 1, one character at a time. The time delay
       between each character being printed is stored as 't'."""
    
    print string_1,
    
    for char in string_2:
        sys.stdout.write(char)
        sys.stdout.flush()
        time.sleep(t)
        
    print ""

main()
#start()    
    
    
    
    

